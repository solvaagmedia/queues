package no.uib.info233.v2016.queues;

/**
 * This interface is based on the ADT Queue. 
 * 
 * @author Sigve
 *
 * @param <T>
 */


public interface QueueInterface<T> {
	
	/**
	 * Returns true if the queue is empty, otherwise it should only return false.
	 * 
	 * @return boolean result, which is true if there is no enqueued item. 
	 */
	boolean isEmpty();
	
	
	/**
	 * This method should return the front item, but not dequeue it. 
	 * 
	 * @return The data of the front object. 
	 * @throws QueueException if there is no object in the queue. 
	 */
	T peek() throws QueueException;
	
	
	/**
	 * This method inserts objects at the end of the queue. 
	 * 
	 * @param data - Data which the queue is parameterized to store. 
	 * @throws QueueException if the data could not be added to the queue. 
	 */
	void enqueue(T data) throws QueueException;
	
	
	/**
	 * This method should return the first object in the queue. 
	 * 
	 * @return An object of the type the queue is parameterized to. 
	 * @throws QueueException if there is no object to dequeue. 
	 */
	T dequeue() throws QueueException;
	
	
	/**
	 * This method should universally delete the queue, regardless of the queue's state.
	 */
	void clear();
	
	/**
	 * This method should return the number of objects stored in the queue. 
	 * 
	 * @return an integer representation of the amount of objects in the queue. 
	 */
	int size();

}
